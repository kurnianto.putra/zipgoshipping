import styled, { keyframes } from "styled-components";
import Image from "next/image";
import UseImageRatio from "../ShareComponent/ImageRatio";
const fadeSlideDown = keyframes`
0%{
  opacity: 0;
  transform: translateY(-4rem);
}
100 {
  opacity: 1;
  transform: none;
}
`;
const Box = styled.header`
  position: absolute;
  z-index: 2;
  width: 100%;
  &::after {
    content: "";
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    height: 100vh;
    width: 100%; /* set to 100% for full overlay width */
    /* background: linear-gradient(
      176.75deg,
      #2a2626 2.69%,
      rgba(221, 55, 55, 0) 136.71%
    ); */
    background: linear-gradient(
      331deg,
      rgba(2, 40, 87, 0.7889355571330094) 0%,
      rgba(221, 10, 10, 0.5) 100%
    );
  }
  img {
    background-size: cover;
    animation: ${fadeSlideDown} 2s ease-out forwards;
  }
`;

function BackgroundImage({ children }) {
  const { width, height } = UseImageRatio();

  if (width && height) {
    return (
      <Box>
        {children}
        <Image src={"/enam.jpg"} width={width} height={height} />
      </Box>
    );
  }

  return null;
}
export default BackgroundImage;
