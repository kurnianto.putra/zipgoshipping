import { Description, HeaderTitle } from "../../styles/Text";
import { FlexContainer, FlexRow } from "../../styles/Wrapper";
import styled from "styled-components";
import dynamic from "next/dynamic";
import { getLinkWhastapp } from "../../utils/fn";
import { useState } from "react";
import SearchBox from "./SearchBox";
const BgImage = dynamic(() => import("./BackgroundImage"), {
  ssr: false,
});

export const Header = styled.header`
  position: relative;
  height: 100vh;
  background: ${({ theme }) => theme.primary};
  z-index: 1;
  width: 100%;
`;
export const Section = styled.section`
  z-index: 3;
  width: 100%;
  height: 100vh;
  position: absolute;
  left: 0;
  color: #fff;
  background: linear-gradient(
    176.75deg,
    #2a2626 2.69%,
    rgba(221, 55, 55, 0) 136.71%
  );
`;

function Home() {
  const [isVisible, setIsVisible] = useState(false);
  function sendWa() {
    getLinkWhastapp(
      "81372640767",
      "Assalamaualaiakum dengan kami perusahaan logistic zapgo"
    );
  }

  function openModal() {
    setIsVisible(true);
  }
  return (
    <Header>
      <Section>
        <FlexContainer
          aitem={"center"}
          height={"100vh"}
          jcontent={"center"}
          overflow={"visible"}
          phone_flexDir={"column"}
          phone_jcontent={"flex-start"}
        >
          <FlexRow
            margin={"25px"}
            phone_padding={"15px"}
            size={5}
            height={"auto"}
          >
            <HeaderTitle
              letterSpac={"10px"}
              textAlign={"left"}
              fs={"1rem"}
              fw={"600"}
              margin={"auto"}
              pb={"10px"}
              lineHeight={"25px"}
              phone_width={"100%"}
              phone_fs={"1rem"}
            >
              ZAPGO
            </HeaderTitle>
            <HeaderTitle
              textAlign={"left"}
              fs={"3.7rem"}
              fw={"600"}
              textTransform={"uppercase"}
              margin={"auto"}
              lineHeight={"65px"}
              pb={"40px"}
              phone_width={"100%"}
              phone_fs={"2rem"}
              phone_lineHeight={"1.5"}
              phone_pb={"10px"}
            >
              Worldwide Goods Shipping Service
            </HeaderTitle>
          </FlexRow>
          <FlexRow
            display={"flex"}
            height={"auto"}
            bgColor={"#ffc344"}
            size={5}
            margin={"25px"}
            bradius={"5px"}
            shadow
            phone_height={"auto"}
          >
            <SearchBox />
          </FlexRow>
        </FlexContainer>
      </Section>
      <BgImage />
    </Header>
  );
}

export default Home;

// import { Description, HeaderTitle } from "../../styles/Text";
// import { FlexContainer, FlexRow } from "../../styles/Wrapper";
// import dynamic from "next/dynamic";
// import styled from "styled-components";
// import { getLinkWhastapp } from "../../utils/fn";
// import { useState } from "react";
// import SearchBox from "./SearchBox";
// import SlideShow from "./SlideShow";
// const BgImage = dynamic(() => import("./BackgroundImage"), {
//   ssr: false,
// });

// const Header = styled.header`
//   position: relative;
//   height: 100vh;
//   background: ${({ theme }) => theme.yellow};
//   z-index: 1;
//   width: 100%;
// `;
// const Section = styled.section`
//   z-index: 3;
//   width: 100%;
//   height: 100vh;
//   left: 0;
//   color: #fff;
//   background: ${({ theme }) => theme.yellow};
// `;

// function Home() {
//   const [isVisible, setIsVisible] = useState(false);
//   function sendWa() {
//     getLinkWhastapp(
//       "81372640767",
//       "Assalamaualaiakum dengan kami perusahaan logistic zapgo"
//     );
//   }

//   function openModal() {
//     setIsVisible(true);
//   }
//   return (
//     <Header>
//       <Section>
//         <FlexContainer
//           aitem={"center"}
//           height={"100vh"}
//           overflow={"visible"}
//           phone_flexDir={"column"}
//         >
//           <FlexRow size={7}>
//             <SlideShow />
//           </FlexRow>
//           <FlexRow
//             display={"flex"}
//             height={"100vh"}
//             bgColor={"#ffc344"}
//             size={5}
//             bradius={"5px"}
//             shadow
//             phone_height={"auto"}
//           >
//             <SearchBox />
//           </FlexRow>
//         </FlexContainer>
//       </Section>
//     </Header>
//   );
// }

// export default Home;
{
  /* 
            <FlexRow
            size={8}
            height={"auto"}
            display={"flex"}
            flexDir={"column"}
            jcontent={"center"}
          >
            <HeaderTitle
              textAlign={"center"}
              fs={"3.7rem"}
              fw={"600"}
              width={"850px"}
              margin={"auto"}
              lineHeight={"65px"}
              pb={"40px"}
              phone_width={"100%"}
              phone_fs={"2rem"}
              phone_lineHeight={"1.5"}
              phone_pb={"10px"}
            >
              Worldwide Goods Shipping Service
            </HeaderTitle>
            <Description
              margin={"auto"}
              width={"400px"}
              textAlign={"center"}
              phone_width={"100%"}
              phone_pb={"20px"}
            >
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s.
            </Description>
            <FlexRow mtop={"30px"} display={"flex"} jcontent={"center"}>
              <Button onClick={openModal} textColor={"#fff"}>
                <IconCarLogistic color={"#fff"} stroke={1.4} />
                Cek Harga
              </Button>
              <Button textColor={"#fff"} ml={"15px"} onClick={() => sendWa()}>
                <IconCall color={"#fff"} stroke={1.4} />
                Hubungi Kami
              </Button>
            </FlexRow>
          </FlexRow>
  
  */
}
