import { useEffect, useState } from "react";
import { Input, WrapInput, WrapperSearch, WrappIcon } from "../../styles/Input";
import { FlexContainer, FlexRow } from "../../styles/Wrapper";
import Button from "../ShareComponent/Button";
import { useForm } from "react-hook-form";
import {
  IconCall,
  IconJenis,
  IconEmail,
  IconPlace,
  IconUser,
  IconWhatsApp,
} from "../ShareComponent/Icon";
import { useTheme } from "styled-components";
import { checkPrice, searchAllValue } from "../../utils/fn";
function SearchBox() {
  const [negara, setNegara] = useState([])
  const theme = useTheme();
  const [query, setQuery] = useState("")
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors, isValid },
  } = useForm({ mode: "onChange" });
  const [show, setShow] = useState(false)

  const fecthData = async () => {
    fetch(`${process.env.NEXT_PUBLIC_API}/negara`)
      .then((response) => response.json())
      .then((json) => setNegara(json))
      .catch((err) => console.error("error get data", err));
  };

  async function onSubmit(values) {
    console.log("query", query)
    checkPrice("811198929", values , query);
    reset();
  }


  useEffect(() => {
    let isFetch = true
    if(isFetch){
      fecthData();
    }
    return () => isFetch =false
  }, []);
  function onChangeNegara(e){
    setQuery(e.target.value)
    setShow(true)
  }

  function simpanPilihan(data){
    setQuery(data)
    setShow(false)
  }
 
  return (
    <FlexContainer
      style={{ margin: "auto" }}
      flexDir={"column"}
      height={"auto"}
      padding={"10px"}
      jcontent={"space-between"}
    >
      <fieldset>
        <legend>Form Check Harga Pengiriman</legend>
        <form onSubmit={handleSubmit(onSubmit)}>
          <FlexRow display={"flex"} phone_flexWrap={"wrap"}>
            <WrapInput width={"100%"}>
              <WrappIcon>
                <IconUser color={"#4C4C4C"} />
              </WrappIcon>
              <Input
                type="text"
                name="username"
                placeholder="Nama Pengirim"
                {...register("username", {
                  required: "This input is required",
                })}
              />
              {errors.username && <label>{errors.username.message}</label>}
            </WrapInput>
            <WrapInput width={"100%"}>
              <WrappIcon>
                <IconEmail color={"#4C4C4C"} />
              </WrappIcon>
              <Input
                name="email"
                placeholder="Email"
                {...register("email", {
                  required: "This input is required",
                  pattern: {
                    value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{1,64}$/i,
                    message: "Please enter a valid email",
                  },
                })}
              />
              {errors.email && <label>{errors.email.message}</label>}
            </WrapInput>
          </FlexRow>
          <FlexRow display={"flex"}>
            <WrapInput phoneWidth={"100%"} width={"100%"}>
              <WrappIcon>
                <IconCall stroke={"2"} color={"#4C4C4C"} />
              </WrappIcon>
              <Input
                placeholder="Nomor Hp"
                name="nomor_hp"
                {...register("nomor_hp", {
                  required: "This input is required",
                  pattern: {
                    value: /\d+/i,
                    message: "This input is number only.",
                  },
                  maxLength: {
                    value: 12,
                    message: "This input exceed max length.",
                  },
                })}
              />
              {errors.nomor_hp && <label>{errors.nomor_hp.message}</label>}
            </WrapInput>
          </FlexRow>
          <FlexRow display={"flex"}>
            <WrapInput width={"100%"}>
              <WrappIcon>
                <IconPlace color={"#4C4C4C"} />
              </WrappIcon>
              <Input
                name="tujuan"
                placeholder="Negara Tujuan"
                value={query}
                onChange={(e) =>onChangeNegara(e)}
              />
              
              {show && query !== "" ? <WrapperSearch>
                {
                searchAllValue(negara.response,"nama_negara",query)?.length !== 0 ?
                searchAllValue(negara.response,"nama_negara",query)?.map((el,index)=>{
                  return(
                    <li key={index} onClick={()=>simpanPilihan(el.nama_negara)}>{el.nama_negara}</li>
                  )
                }): 
              <li onClick={()=>simpanPilihan("Lainnya")}>Lainnya</li>
                }
              </WrapperSearch> : 
                null
              }
            </WrapInput>
          </FlexRow>
          <FlexRow display={"flex"}>
            <WrapInput width={"100%"}>
              <WrappIcon>
                <IconJenis color={"#4c4c4c"} />
              </WrappIcon>
              <Input
                name="jenis"
                placeholder="Jenis Barang"
                {...register("jenis", {
                  required: "This input is required",
                })}
              />
              {errors.jenis && <label>{errors.jenis.message}</label>}
            </WrapInput>
          </FlexRow>
          <FlexRow display={"flex"}>
            <WrapInput width={"50%"}>
              <Input
                padLeft={"5px"}
                phone_padLeft={"5px"}
                name="berat"
                placeholder="Berat (kg)"
                {...register("berat", {
                  required: "This input is required",
                })}
              />
              {errors.jenis && <label>{errors.jenis.message}</label>}
            </WrapInput>
            <WrapInput width={"50%"}>
              <Input
                padLeft={"5px"}
                phone_padLeft={"5px"}
                name="panjang"
                placeholder="P (cm)"
                {...register("panjang", {
                  required: "This input is required",
                })}
              />
              {errors.jenis && <label>{errors.jenis.message}</label>}
            </WrapInput>
            <WrapInput width={"50%"}>
              <Input
                padLeft={"5px"}
                phone_padLeft={"5px"}
                name="lebar"
                placeholder="L (cm)"
                {...register("lebar", {
                  required: "This input is required",
                })}
              />
              {errors.jenis && <label>{errors.jenis.message}</label>}
            </WrapInput>
            <WrapInput width={"50%"}>
              <Input
                padLeft={"5px"}
                phone_padLeft={"5px"}
                name="tinggi"
                placeholder="T (cm)"
                {...register("tinggi", {
                  required: "This input is required",
                })}
              />
              {errors.jenis && <label>{errors.jenis.message}</label>}
            </WrapInput>
          </FlexRow>
          <FlexRow display={"flex"}>
            <Button
              marginTop={"10px"}
              width={"100%"}
              color={theme.secondary}
              text={theme.yellow}
              disabled={!isValid}
              style={{ cursor: isValid ? "pointer" : "not-allowed" }}
            >
              Submit&nbsp;&nbsp;
              <IconWhatsApp color={theme.yellow} />
            </Button>
          </FlexRow>
        </form>
      </fieldset>
    </FlexContainer>
  );
}
export default SearchBox;
