import { FlexContainer, FlexRow } from "../../styles/Wrapper";
import { WrapperCardScroll, Card } from "../../styles/Card";
import TitleHeader from "../ShareComponent/TitleHeader";

function Keunggulan() {
  return (
    <FlexContainer flexDir={"column"} height={"auto"} phone_width={"100%"}>
      <FlexRow size={11.9}>
        <TitleHeader
          title={"Benefits"}
          desc={"Intip bagaimana proses pengiriman ZAPGO"}
        />
      </FlexRow>
      <FlexRow size={12}>
        <WrapperCardScroll>
          <Card>
            <h2>Fast</h2>
            <p>Fast Respond, Fast Delivered to or from anywhere goods.</p>
            <img src="/clock.svg" alt="packing" />
          </Card>
          <Card>
            <h2>Easy</h2>
            <p>A simple process.</p>
            <img src="/delivery-man.svg" alt="packing" />
          </Card>
          <Card>
            <h2>Wide Coverage</h2>
            <p>We coverage to most of all cities.</p>
            <img
              src="/global.svg"
              alt="https://www.flaticon.com/authors/pixel-perfect"
            />
          </Card>
          <Card>
            <h2>Efficient</h2>
            <p>We will pick and deliver your goods to your front door.</p>
            <img
              src="/effi.svg"
              alt="https://www.flaticon.com/authors/xnimrodx"
            />
          </Card>
          <Card>
            <h2>Competitive Price</h2>
            <p>Our price affordable in market price range</p>
            <img src="/best-price.svg" alt="https://www.freepik.com" />
          </Card>
          <Card>
            <h2>No MOQ</h2>
            <p>No minimum weight</p>
            <img
              src="/scale.svg"
              alt="https://www.flaticon.com/authors/eucalyp"
            />
          </Card>
          <Card>
            <h2>UMKM Friendly</h2>
            <p>Please check with our Customer Service for UMKM Package</p>
            <img src="/friends.svg" alt="packing" />
          </Card>
          <Card>
            <h2>Fast Respond</h2>
            <p>
              Our Communicative customer Service ready to help you with your
            </p>
            <img src="/clock.svg" alt="packing" />
          </Card>
        </WrapperCardScroll>
      </FlexRow>
    </FlexContainer>
  );
}

export default Keunggulan;

{
  /* 
/> */
}
