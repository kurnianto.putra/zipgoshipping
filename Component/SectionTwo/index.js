import { FlexContainer, FlexRow } from "../../styles/Wrapper";
import TitleHeader from "../ShareComponent/TitleHeader";
import styled from "styled-components";
import { WrapperCard } from "../../styles/Card";
import { media } from "../../styles/Media";
import {
  IconApproved,
  IconBoxRecevied,
  IconCalculation,
  IconContactUs,
  IconPayment,
  IconReceivedData,
  IconUser,
  IconVerification,
} from "../ShareComponent/Icon";
const StyleCardPengiriman = styled.div`
  border-radius: 5px;
  box-shadow: 0px 9px 43px -20px rgba(194, 135, 17, 0.7);
  padding: 10px;
  position: relative;
  width: 200px;
  height: 150px;
  background-color: #fff;
  margin: 20px;
  display: flex;
  flex-direction: column;
  cursor: pointer;
  justify-content: center;
  .number {
    padding: 4px 20px;
    background-color: ${({ theme }) => theme.primary};
    color: #fff;
    font-size: 1.2rem;
    position: absolute;
    top: -10px;
    left: -10px;
    font-weight: 500;
    border-radius: 10px 5px;
  }
  svg {
    margin: auto;
    height: ${(p) => p.height || "70px"};
    width: ${(p) => p.width || "70px"};
  }
  p {
    height: 100px;
    width: 100%;
    margin: 0 auto;
    line-height: 1.4;
    padding-top: 5px;
    color: ${({ theme }) => theme.secondary};
    text-align: center;
  }
  &:hover {
    transform: scale(1.06);
    transition: transform 0.9s ease-out;
  }

  ${media.phone`
    width: 150px;
    margin: 15px;

    .number {
    padding: 4px 10px;
    color: #fff;
    font-size: 1rem;
    position: absolute;
  }
    img {
    margin: auto;
    width: 40px;
  }
  p {
    line-height: 1.2;
  }
  `}
`;
function ProsesPengiriman() {
  return (
    <FlexContainer
      flexDir={"column"}
      phone_width={"100%"}
      height={"auto"}
      phone_height={"auto"}
    >
      <FlexRow size={11.9}>
        <TitleHeader
          title={"Delivery Process"}
          desc={"Bagaimana Proses Pengiriman di ZAPGO"}
        />
      </FlexRow>

      <WrapperCard width={"80%"}>
        <StyleCardPengiriman>
          <span className="number">1</span>
          <IconContactUs />
          <p>Contact US</p>
        </StyleCardPengiriman>
        <StyleCardPengiriman>
          <span className="number">2</span>
          <IconReceivedData />
          <p>Recevied data from customer</p>
        </StyleCardPengiriman>
        <StyleCardPengiriman>
          <span className="number">3</span>
          <IconApproved />
          <p>Approved</p>
        </StyleCardPengiriman>
        <StyleCardPengiriman>
          <span className="number">4</span>
          <IconCalculation />
          <p>Calculate the price</p>
        </StyleCardPengiriman>
        <StyleCardPengiriman>
          <span className="number">5</span>
          <IconPayment />
          <p>Make a Payment</p>
        </StyleCardPengiriman>
        <StyleCardPengiriman>
          <span className="number">6</span>
          <IconVerification />
          <p>Verification</p>
        </StyleCardPengiriman>
        <StyleCardPengiriman>
          <span className="number">6</span>
          <IconBoxRecevied />
          <p>Enjoy ur package recevied</p>
        </StyleCardPengiriman>
      </WrapperCard>
    </FlexContainer>
  );
}
export default ProsesPengiriman;
