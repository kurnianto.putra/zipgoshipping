import { useEffect, useRef, useState } from "react";
import styled from "styled-components";
import Image from "next/image";
import { media } from "../../styles/Media";
import Dot from "../ShareComponent/Dot";

const WrapSlider = styled.div`
  width: 70%;
  margin: auto;
  background-color: #fff;
  display: flex;
  flex-direction: column;
  position: relative;
  justify-content: center;
  align-content: center;
  align-items: center;
  padding: 10px;
  ${media.phone`
    height:50vh;
    width: 100%;
  `}
`;
const BoxSlide = styled.div`
  position: relative;
  box-shadow: 29px 18px 69px 0px rgba(255, 255, 255, 0.26);
  display: flex;
  width: 100%;
  border-radius: 10px;
  height: 100%;
  img {
    background-size: cover;
    border-radius: 5px;
  }

  ${media.phone`
    height: 35vh;
  `}
`;

function getWindowDimensions() {
  const { innerWidth: width, innerHeight: height } = window;
  return {
    width,
    height,
  };
}

function SlideShow() {
  const slides = ["satu", "tiga", "empat", "lima"];
  const [width, setWidth] = useState(Number());
  const [height, setheight] = useState(Number());
  const [index, setIndex] = useState(0);
  const [auto, setAuto] = useState(true);

  const timer = useRef(null);

  useEffect(() => {
    if (timer.current) {
      clearTimeout(timer.current);
    }
    timer.current = setTimeout(() => {
      if (auto) {
        prevState();
      }
    }, 5000);
    return () => clearTimeout(timer.current);
  }, [auto, index]);

  function prevState() {
    index === slides.length - 1 ? setIndex(0) : setIndex(index + 1);
  }
  useEffect(() => {
    const { width, height } = getWindowDimensions();
    setWidth(width);
    setheight(height);
  }, []);

  useEffect(() => {
    function handleResize() {
      const { width, height } = getWindowDimensions();
      setWidth(width);
      setheight(height);
    }
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);
  if (width && height) {
    return (
      <WrapSlider>
        <BoxSlide>
          <Image src={`/${slides[index]}.jpg`} width={width} height={height} />
        </BoxSlide>
        <Dot slides={slides} activeIndex={index} />
      </WrapSlider>
    );
  }
  return null;
}

export default SlideShow;
