import { FlexContainer, FlexRow } from "../../styles/Wrapper";
import SlideShow from "./slideshow";

function PromoWebsite() {
  return (
    <FlexContainer
      flexDir={"column"}
      phone_width={"100%"}
      height={"auto"}
      background={"#fff"}
      phone_height={"auto"}
    >
      <FlexRow size={12}>
        <SlideShow />
      </FlexRow>
    </FlexContainer>
  );
}
export default PromoWebsite;
