import Head from "next/head";
import { Footer } from "./ShareComponent/Footer";
import Navbar from "./ShareComponent/Navbar";
import styled from "styled-components";
import { Divider } from "../styles/Text";

const Wrapper = styled.div`
  display: grid;
  grid-template-rows: auto 50px;
`;
function Layout({
  children,
  title = "Ekspidisi ZAPGO",
  desc = "ZAPGO ini terbentuk dikarenakan kita ini awalnya hanya sekelompok anak muda yang mempunyai impian untuk membuat negara Indonesia ini untuk maju di bidang ekspor. Semua itu kami lakukan dikarenakan ekspor kita mempunyai banyak potensi untuk dikembangkan menjadi lebih besar, oleh karena itu kita membuat layanan jasa pengiriman ini untuk membantu pihak umkm ini menjadi lebih besar lagi secara global",
}) {
  return (
    <>
      <Head>
        <title>{title}</title>
        <link rel="shortcut icon" href="/zapgo.ico" />
        <link
          rel="icon"
          type="image/png"
          href="/favicon-16x16.png"
          sizes="16x16"
        />
        <link
          rel="icon"
          type="image/png"
          href="/favicon-32x32.png"
          sizes="32x32"
        />
        <link
          rel="icon"
          type="image/png"
          href="/android-192x192.png"
          sizes="192x192"
        />
        <link
          rel="apple-touch-icon"
          href="/apple-touch-icon-180x180.png"
          sizes="180x180"
        />
        <meta name="description" content={desc} />
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Poppins:100,300,400,500,800"
        />
      </Head>
      <Navbar />
      {children}
      <Wrapper />
      <Footer />
    </>
  );
}

export default Layout;
