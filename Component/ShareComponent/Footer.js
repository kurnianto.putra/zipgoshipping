import styled, { useTheme } from "styled-components";
import { media } from "../../styles/Media";
import { Description, HeaderTitle } from "../../styles/Text";
import { FlexRow } from "../../styles/Wrapper";

const FooterStyle = styled.footer`
  background-color: ${({ theme }) => theme.primary};
  height: auto;
  display: flex;
  display: row;
  justify-content: center;
  padding: 10px;

  ${media.phone`
  justify-content : flex-start;
  flex-direction : column;
  height: 500px;

  `}
`;

export function Footer() {
  const theme = useTheme();
  return (
    <FooterStyle>
      <FlexRow size={3} margin={"10px"}>
        <HeaderTitle fw={"500"} fs={"1.7rem"}>
          Office
        </HeaderTitle>
        <Description color={theme.textColorS}>
          New Harco Glodok Lantai 3 Blok F 15, Kota Tua.
        </Description>
        <Description color={theme.textColorS}>
          Jl. Hayam Wuruk No.9 RT 1/RW 6, RT 1, RW.6, Mangga Besar
        </Description>
        <Description color={theme.textColorS}>
          Kec. Taman Sari, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta
          11180
        </Description>
      </FlexRow>
      <FlexRow size={2} margin={"10px"}>
        <HeaderTitle fw={"500"} fs={"1.7rem"}>
          Informasi
        </HeaderTitle>
        <Description color={theme.textColorS}>FAQ</Description>
        <Description color={theme.textColorS}>Term And Condition</Description>
      </FlexRow>{" "}
      <FlexRow size={2} margin={"10px"}>
        <HeaderTitle fw={"500"} fs={"1.7rem"}>
          Hubungi Kami
        </HeaderTitle>
        <Description color={theme.textColorS}>Whatsapp</Description>
        <Description color={theme.textColorS}>Instagram</Description>
        <Description color={theme.textColorS}>Facebook</Description>
      </FlexRow>
      <FlexRow size={2} margin={"10px"}>
        <HeaderTitle fw={"500"} fs={"1.7rem"}>
          Jam Kantor
        </HeaderTitle>
        <Description color={theme.textColorS}>
          - 09.00 - 17.00 WIB Monday-Friday*
        </Description>
        <Description color={theme.textColorS}>
          - 09.00 - 17.00 WIB Saturday*
        </Description>
        <Description color={theme.textColorS}>- Sunday (Closed)</Description>
        <Description color={theme.textColorS}>
          - di luar jam tersebut late respond. Terima kasih.
        </Description>
      </FlexRow>
    </FooterStyle>
  );
}
