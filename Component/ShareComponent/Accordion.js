import React, { useState } from "react";
import styled from "styled-components";
import { media } from "../../styles/Media";

const AccordionSection = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  position: relative;
  height: auto;
  width: 100%;
`;

const Container = styled.div`
  box-shadow: 2px 10px 35px 1px rgba(153, 153, 153, 0.3);
  width: 100%;
`;

const Wrap = styled.div`
  background: ${({ theme }) => theme.primary};
  margin-bottom: 5px;
  color: #fff;
  border-radius: 5px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  height: 80px;
  text-align: center;
  cursor: pointer;
  h1 {
    padding: 2rem;
    font-weight: 400;
    font-size: 1rem;
    text-align: left;
  }
  span {
    margin-right: 1.5rem;
  }
`;

const Dropdown = styled.div`
  background: ${({ theme }) => theme.bgColor};
  color: ${({ theme }) => theme.textColor};
  width: 100%;
  height: auto;
  display: flex;
  flex-direction: column;
  border-bottom: 1px solid ${({ theme }) => theme.primary};
  p {
    font-size: 0.9rem;
    padding: 10px;
  }

  ${media.phone`
  height:auto;
  `}
`;

const Accordion = ({ data }) => {
  const [clicked, setClicked] = useState(false);

  const toggle = (index) => {
    if (clicked === index) {
      //if clicked question is already active, then close it
      return setClicked(null);
    }

    setClicked(index);
  };

  return (
    <AccordionSection>
      <Container>
        {data?.map((item, index) => {
          return (
            <>
              <Wrap onClick={() => toggle(index)} key={index}>
                <h1>{item.pertanyaan}</h1>
                <span>{clicked === index ? "-" : "?"}</span>
              </Wrap>
              {clicked === index ? (
                <Dropdown>
                  <p>{item.jawaban}</p>
                </Dropdown>
              ) : null}
            </>
          );
        })}
      </Container>
    </AccordionSection>
  );
};

export default Accordion;
