import styled from "styled-components";

const WrapeDot = styled.div`
  position: absolute;
  bottom: 25px;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const StyleDots = styled.div`
  padding: 3px;
  width: 10px;
  margin-right: 5px;
  cursor: pointer;
  border-radius: 5px;
  background: ${(p) => (p.active ? "#ffc344" : "#f2f2f2")};
`;

function Dot({ slides, activeIndex }) {
  return (
    <WrapeDot>
      {slides?.map((slide, i) => (
        <StyleDots key={slide} active={activeIndex === i} />
      ))}
    </WrapeDot>
  );
}
export default Dot;
