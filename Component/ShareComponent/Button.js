import styled, { keyframes } from "styled-components";
import { media } from "../../styles/Media";

const shakeAnimation = keyframes`
	80% {
		opacity: 0;
		transform: scale(2);
	}
	100% {
		opacity: 0;
		transform: scale(2);
	}
`;

const StyleButton = styled.button`
  // Reset
  text-decoration: none;
  border: none;
  cursor: ${(p) => (p.cursor ? "pointer" : "not-allowed")};
  margin-left: ${(p) => p.ml};
  //display
  display: inline-flex;
  width: ${(p) => p.width};
  align-items: center;
  justify-content: center;
  align-self: flex-start;
  text-align: center;
  margin-top: ${(p) => p.marginTop};
  //visual
  font-size: 0.92rem;
  background: ${({ color, theme }) => color || theme.primary};
  line-height: 1.1;
  color: ${(p) => p.text || "#fff"};
  border-radius: ${(p) => p.border_radius || "5px"};
  box-shadow: 0 3px 5px 0 rgba(0, 0, 0, 0.18);
  -webkit-transition: all 0.5s;
  -moz-transition: all 0.5s;
  transition: all 0.5s;
  &:not(svg) {
    padding: 0.2em 0.7em;
    min-width: 10ch;
    min-height: 40px;
  }

  &:active {
  }

  &:focus {
    outline: none;
  }

  svg {
    width: 2rem;
    height: 2rem;
    padding: 0.35em;
  }

  &:active > svg {
    outline: none;
    animation: ${shakeAnimation} 0.4s ease-out;
  }

  &:hover {
    box-shadow: 0 6px 5px 0 rgba(0, 0, 0, 0.18);
  }

  ${media.phone`
  
  
  `}
`;

function Button({ children, ...args }) {
  return (
    <StyleButton {...args} cursor={true}>
      {children}
    </StyleButton>
  );
}
export default Button;
