import { useEffect, useState } from "react";
function getWindowDimensions() {
  const { innerWidth: width, innerHeight: height } = window;
  return {
    width,
    height,
  };
}
function UseImageRatio() {
  const [width, setWidth] = useState(Number());
  const [height, setheight] = useState(Number());

  useEffect(() => {
    const { width, height } = getWindowDimensions();
    setWidth(width);
    setheight(height);
  }, []);

  useEffect(() => {
    function handleResize() {
      const { width, height } = getWindowDimensions();
      setWidth(width);
      setheight(height);
    }
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  return { width, height };
}

export default UseImageRatio;
