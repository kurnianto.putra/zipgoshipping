import React, { useState, useEffect } from "react";
import styled, { useTheme } from "styled-components";
import Burger from "./Burger";
import Image from "next/image";
const Nav = styled.div`
  z-index: 2;
  position: fixed;
  width: 100%;
  background-color: ${({ theme, bgColor }) =>
    bgColor ? theme.yellow : "none"};
  height:  ${({ bgColor }) =>
    bgColor ? "50px" : "90px"};
  overflow: hidden;
  box-shadow: ${({ bgColor }) =>
    bgColor ? "0px 4px 19px rgba(194,132,17,0.69)" : "none"};
  padding: 0 20px;
  align-items: center;
  transition: all 0.3s linear;
  display: flex;
`;

const Logo = styled.div`
  padding: 40px 10px;
  overflow: hidden;
  display: flex;
  align-items: center;
  align-content: center;
  margin-bottom: -10px;
  .logoza {
    font-weight: 600;
    font-size: 1.5rem;
    margin: auto;
    align-items: center;
    align-items: center;
    padding: 4px 5px;
    margin-top: -10px;
    letter-spacing: 2px;
    color: ${({ theme }) => theme.secondary};
  }
  img{
    width:100px;
    padding: 104px;
    height: 70px;
    border:1px solid red;
  }
`;

const Navbar = () => {
  const theme = useTheme();
  const [bgColor, setBgColor] = useState(false);
  const changeBgNav = () => {
    if (typeof window !== "undefined") {
      window.scrollY >= 40 ? setBgColor(true) : setBgColor(false);
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", changeBgNav);
    return () => {
      window.removeEventListener("scroll", changeBgNav);
    };
  }, [bgColor]);

  return (
    <Nav bgColor={bgColor}>
      <Logo>
        {bgColor ? (
          <div className="logoza">ZAPGO</div>
        ) : (
          <Image src="/logo.png" width={"250px"} height={"250px"} alt="logo" />
        )}
      </Logo>
      <Burger
        hoverColor={bgColor ? theme.primary : theme.primary}
        color={bgColor ? theme.secondary : theme.yellow}
      />
    </Nav>
  );
};

export default Navbar;
