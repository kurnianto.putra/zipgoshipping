import React from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import styled from "styled-components";
import { media } from "../../styles/Media";

const Menu = styled.ul`
  z-index: -1;
  display: flex;
  list-style: none;
  flex-flow: row nowrap;
  justify-content: flex-end;
  li > a {
    padding: 18px 10px;
    color: ${({ color, theme }) => color || theme.textColorS};
    cursor: pointer;
    margin-left: 15px;
    position: relative;
    font-size: 16px;
    &:hover {
      color: ${({ hoverColor, theme }) => hoverColor || theme.primary};
      transition: all 0.1s ease-out;
      border-bottom: 3px solid ${({ theme }) => theme.primary};
      font-size: 16px;
      z-index: 2;
    }
  }
  li > a.active::before {
    font-weight: 600;
  }
  li > a.active::before {
    position: absolute;
    content: "";
    width: 10px;
    height: 10px;
    border-radius: 50px;
    background: ${({ color, theme }) => color || theme.textColorS};
    left: -2px;
  }
  width: 100%;
  ${media.phone`
     flex-flow : column nowrap;
     background : ${({ theme }) => theme.primary};
     top : 0;
     right : 0 ;
     height : auto;
     position: fixed;
     padding-top: 2rem;
     z-index:-1;
     margin-top:-10px;
     text-align : center;
     color : white;
     transform: ${({ open }) => (open ? "translateX(0)" : "translateX(100%)")};
     transition: transform 0.3s ease-in-out;
    box-shadow :  0px 4px 19px rgba(255, 98, 32, 0.24);
     li{
       padding : 20px;
     }

     li > a {
       color : ${({ theme }) => theme.yellow};

       &:hover{
        color : ${({ theme }) => theme.secondary}
       }
      
     }

     li > a.active {
    font-weight: 600;
  }

  `}
`;

const RightNav = ({ open, color, hoverColor }) => {
  const router = useRouter();

  return (
    <Menu open={open} color={color} hoverColor={hoverColor}>
      <li>
        <Link href="/">
          <a className={router.pathname === "/" ? "active" : ""}>Home</a>
        </Link>
      </li>
      <li>
        <Link href="/about">
          <a className={router.pathname === "/about" ? "active" : ""}>
            About Us
          </a>
        </Link>
      </li>
      <li>
        <Link href="/information">
          <a className={router.pathname == "/information" ? "active" : ""}>
            Information
          </a>
        </Link>
      </li>
      <li>
        <Link href="/tracking">
          <a className={router.pathname == "/tracking" ? "active" : ""}>
            Tracking
          </a>
        </Link>
      </li>
    </Menu>
  );
};

export default RightNav;
