import React, { useState } from "react";
import styled from "styled-components";
import RightNav from "./RightNav";

const StyledBurger = styled.div`
  z-index: 5;
  background-color: ${({ open, theme }) => (open ? "none" : theme.primary)};
  padding: 2px;
  width: 2.3rem;
  height: 2rem;
  top: 15px;
  right: 50px;
  border-radius: 5px;
  position: absolute;
  display: none;
  @media (max-width: 768px) {
    display: flex;
    justify-content: space-around;
    flex-flow: column nowrap;
  }
`;

const LineBurger = styled.div`
  width: 2rem;
  height: 0.25rem;
  background-color: ${({ open, theme }) =>
    open ? theme.yellow : theme.yellow};
  border-radius: 10px;
  transform-origin: 1px;
  transition: all 0.3s linear;
  &:nth-child(1) {
    transform: ${({ open }) => (open ? "rotate(42deg)" : "rotate(0)")};
  }
  &:nth-child(2) {
    transform: ${({ open }) => (open ? "translateX(100%)" : "translateX(0)")};
    opacity: ${({ open }) => (open ? 0 : 1)};
  }
  &:nth-child(3) {
    transform: ${({ open }) => (open ? "rotate(-40deg)" : "rotate(0)")};
  }
`;

const Burger = ({ color, hoverColor }) => {
  const [open, setOpen] = useState(false);

  return (
    <>
      <StyledBurger open={open} onClick={() => setOpen(!open)}>
        <LineBurger open={open} />
        <LineBurger open={open} />
        <LineBurger open={open} />
      </StyledBurger>
      <RightNav open={open} color={color} hoverColor={hoverColor} />
    </>
  );
};

export default Burger;
