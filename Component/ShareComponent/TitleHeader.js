import { useTheme } from "styled-components";
import { HeaderTitle } from "../../styles/Text";

function TitleHeader({ title, desc }) {
  const theme = useTheme();
  return (
    <HeaderTitle
      textAlign={"center"}
      fw={"500"}
      fs={"2.1rem"}
      color={theme.textColorP}
    >
      {title}
      <span>{desc}</span>
      <hr />
    </HeaderTitle>
  );
}
export default TitleHeader;
