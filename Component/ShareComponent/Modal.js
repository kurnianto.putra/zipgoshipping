import React, { useEffect, useRef } from "react";
import styled, { css } from "styled-components";
import { media } from "../../styles/Media";

export function useOnClickOutside(ref, handler) {
  useEffect(() => {
    const listener = (event) => {
      // Do nothing if clicking ref's element or descendent elements
      if (!ref.current || ref.current.contains(event.target)) {
        return;
      }

      handler(event);
    };

    document.addEventListener("mousedown", listener);
    document.addEventListener("touchstart", listener);

    return () => {
      document.removeEventListener("mousedown", listener);
      document.removeEventListener("touchstart", listener);
    };
  }, []); // Empty array ensures that effect is only run on mount and unmount
}

const ModalBackground = styled.div`
  position: fixed;
  z-index: 1000 !important;
  top: 0;
  left: 0;
  width: 100%;
  ${({ open }) =>
    open
      ? css`
          background: rgba(1, 0, 0, 0.6);
          display: block;
          z-index: 99999999 !important;
        `
      : css`
          background: rgba(0, 0, 0, 0);
          display: none;
        `}
  height: 100%;
`;

const Modal = styled.div`
  position: relative;
  z-index: 99999999 !important;
  background: #fff;
  width: ${(p) => p.width};
  height: ${(p) => p.tinggi || "auto"};
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  overflow-y: ${(p) => p.scroll};
  border-radius: 10px;
  padding: 30px;
  box-shadow: 0 2px 4px 0 rgba(50, 50, 93, 0.1);

  .button-close {
    position: absolute;
    background-color: ${({ theme }) => theme.primary};
    width: 40px;
    height: 40px;
    border: none;
    top: -1px;
    right: 0;
    cursor: pointer;
    font-size: 1.5rem;
    color: #fff;
    border-radius: 0px 5px 0 5px;
  }
  h3 {
    margin-top: -10px;
    font-weight: 500;
    font-size: 1.5rem;
    text-align: center;
    color: #141323;
  }
  hr {
    margin-top: 5px;
    height: 0.3px;
    background-color: #141323;
  }
  ${media.phone`
      width: 90%;
  `}
  h3 {
    margin-top: -10px;
    font-weight: 500;
    font-size: 1.4rem;
    text-align: center;
    color: #141323;
  }
`;

export default function WrappModal({
  isOpen,
  toggle,
  children,
  judul,
  width,
  tinggi,
  scroll,
  background,
}) {
  return (
    <ModalBackground open={isOpen}>
      <Modal
        background={background}
        width={width}
        tinggi={tinggi}
        scroll={scroll}
      >
        <button className="button-close" onClick={() => toggle(false)}>
          x
        </button>
        <h3>{judul}</h3>
        <hr />
        {children}
      </Modal>
    </ModalBackground>
  );
}
