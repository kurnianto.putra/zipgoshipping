import Layout from "../Component/Layout";
import Home from "../Component/Home";
import SectionTwo from "../Component/SectionTwo";
import Keunggulan from "../Component/SectionThree";
import PromoWebsite from "../Component/Promosi";

export default function Container() {
  return (
    <Layout>
      <Home />
      <PromoWebsite />
      <SectionTwo />
      <Keunggulan />
    </Layout>
  );
}
