import Layout from "../Component/Layout";
import { FlexContainer, FlexRow } from "../styles/Wrapper";
import styled, { useTheme } from "styled-components";
import Image from "next/image";
import UseImageRatio from "../Component/ShareComponent/ImageRatio";
import { Description, HeaderTitle } from "../styles/Text";
import informasi from "../Component/ShareComponent/informasi.json";
import Accordion from "../Component/ShareComponent/Accordion";
import data from "../Component/ShareComponent/faq.json";
const StyleImage = styled.div`
  position: absolute;
  z-index: -1;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  overflow: hidden;
  &::after {
    content: "";
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    height: 100vh;
    width: 100%;
    background: linear-gradient(
      176.75deg,
      rgba(2, 40, 87, 0.7) 10%,
      rgba(221, 10, 10, 0.5) 100%
    );
  }
  img {
    object-fit: cover;
  }
`;

const List = styled.ul`
  list-style: none;
  margin-left: 15px;

  li {
    font-size: 1rem;
    padding: 5px 5px;
    padding-left: 20px;
    position: relative;
    color: ${({ theme }) => theme.textDesc};
    &::before {
      content: "";
      width: 6px;
      height: 6px;
      display: inline-block;
      border-radius: 50%;
      position: absolute;
      background: ${({ theme }) => theme.primary};
      left: 0px;
      top: 15px;
    }
  }
`;

function Tracking() {
  const theme = useTheme();
  const { width, height } = UseImageRatio();
  return (
    <Layout>
      <FlexContainer jcontent={"cente"} flexDir={"column"} height={"100vh"}>
        <FlexRow position={"relative"} height={"100vh"} size={12}>
          <HeaderTitle
            fs={"3rem"}
            fw={"600"}
            phone_fs={"2rem"}
            style={{
              margin: "auto",
              width: "100%",
              textAlign: "center",
              marginTop: "250px",
            }}
          >
            Coming soon
          </HeaderTitle>
          <StyleImage>
            <Image src={"/enam.jpg"} width={width} height={height} />
          </StyleImage>
        </FlexRow>
      </FlexContainer>
    </Layout>
  );
}

export default Tracking;
