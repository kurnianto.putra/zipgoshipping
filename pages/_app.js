import { createGlobalStyle, ThemeProvider } from "styled-components";

export const ZapgoColor = {
  primary: "#a42f44",
  yellow: "#ffc344",
  secondary: "#022857",
  textColorP: "#021935",
  textDesc: "#332F33",
  textColorS: "rgb(202, 201, 201)",
  bgColor: "#efefef",
};

const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    font-family: "Poppins", sans-serif;
  }
  body{
    background: ${({ theme }) => theme.bgColor};
    max-width: 100%;
    overflow-x: hidden;
  }
  a{
    text-decoration: none;
  }

  @media only screen and(-webkit-min-device-pixel-ratio:1.5),
  only screen and(-o-min-device-pixel-ratio:3/2),
  only screen and(min--moz-device-pixel-ratio: 1.5),
  only screen and(min-device-pixel-ratio:1.5){
    html,
    body{
      width: 100%;
      border:4px solid blue;
      overflow-x : hidden
    }
  }

`;

export default function App({ Component, pageProps }) {
  return (
    <ThemeProvider theme={ZapgoColor}>
      <div suppressContentEditableWarning>
        <GlobalStyle />
        <Component {...pageProps} />
      </div>
    </ThemeProvider>
  );
}
