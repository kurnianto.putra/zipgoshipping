import Layout from "../Component/Layout";
import { FlexContainer, FlexRow } from "../styles/Wrapper";
import styled from "styled-components";
import Image from "next/image";
import UseImageRatio from "../Component/ShareComponent/ImageRatio";
import { Description, HeaderTitle } from "../styles/Text";

const StyleImage = styled.div`
  position: absolute;
  z-index: -1;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  overflow: hidden;
  &::after {
    content: "";
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    height: 100vh;
    width: 100%;
    background: linear-gradient(
      176.75deg,
      rgba(2, 40, 87, 0.7) 10%,
      rgba(221, 10, 10, 0.5) 100%
    );
  }
  img {
    object-fit: cover;
  }
`;

function About() {
  const { width } = UseImageRatio();
  return (
    <Layout>
      <FlexContainer jcontent={"cente"} flexDir={"column"} height={"100vh"}>
        <FlexRow position={"relative"} height={"250px"} size={12}>
          <HeaderTitle
            position={"absolute"}
            bottom={"40px"}
            left={"280px"}
            phone_left={"15px"}
            fs={"3rem"}
            fw={"600"}
            margin={"auto"}
            phone_fs={"2.5rem"}
          >
            Tentang ZAPGO
          </HeaderTitle>
          <StyleImage>
            <Image src={"/enam.jpg"} width={width} height={"350px"} />
          </StyleImage>
        </FlexRow>
        <FlexRow size={12}>
          <Description
            margin={"auto"}
            padding={"12px"}
            width={"60%"}
            textAlign={"left"}
            fs={"1rem"}
            phone_width={"100%"}
            phone_pb={"20px"}
          >
            ZAPGO ini terbentuk dikarenakan kita ini awalnya hanya sekelompok
            anak muda yang mempunyai impian untuk membuat negara Indonesia ini
            untuk maju di bidang ekspor. Semua itu kami lakukan dikarenakan
            ekspor kita mempunyai banyak potensi untuk dikembangkan menjadi
            lebih besar, oleh karena itu kita membuat layanan jasa pengiriman
            ini untuk membantu pihak umkm ini menjadi lebih besar lagi secara
            global.
          </Description>
          <Description
            margin={"auto"}
            padding={"12px"}
            width={"60%"}
            textAlign={"left"}
            fs={"1rem"}
            phone_width={"100%"}
            phone_pb={"20px"}
          >
            Dan juga bisa membantu para importir yang ingin memasukan barang ke
            Indonesia juga, yang dapat kita bisa pelajari untuk produk dan pasar
            luar negeri sehingga menambah pengetahuan kita terlebih itu.
          </Description>
        </FlexRow>
      </FlexContainer>
    </Layout>
  );
}

export default About;
