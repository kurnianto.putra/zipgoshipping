import Layout from "../Component/Layout";
import { FlexContainer, FlexRow } from "../styles/Wrapper";
import styled, { useTheme } from "styled-components";
import Image from "next/image";
import UseImageRatio from "../Component/ShareComponent/ImageRatio";
import { Description, HeaderTitle } from "../styles/Text";
import informasi from "../Component/ShareComponent/informasi.json";
import Accordion from "../Component/ShareComponent/Accordion";
import data from "../Component/ShareComponent/faq.json";
const StyleImage = styled.div`
  position: absolute;
  z-index: -1;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  overflow: hidden;
  &::after {
    content: "";
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    height: 100vh;
    width: 100%;
    background: linear-gradient(
      176.75deg,
      rgba(2, 40, 87, 0.7) 10%,
      rgba(221, 10, 10, 0.5) 100%
    );
  }
  img {
    object-fit: cover;
  }
`;

const List = styled.ul`
  list-style: none;
  margin-left: 15px;

  li {
    font-size: 1rem;
    padding: 5px 5px;
    padding-left: 20px;
    position: relative;
    color: ${({ theme }) => theme.textDesc};
    &::before {
      content: "";
      width: 6px;
      height: 6px;
      display: inline-block;
      border-radius: 50%;
      position: absolute;
      background: ${({ theme }) => theme.primary};
      left: 0px;
      top: 15px;
    }
  }
`;

function Informasi() {
  const theme = useTheme();
  const { width } = UseImageRatio();
  return (
    <Layout>
      <FlexContainer jcontent={"cente"} flexDir={"column"} height={"auto"}>
        <FlexRow position={"relative"} height={"250px"} size={12}>
          <HeaderTitle
            position={"absolute"}
            bottom={"40px"}
            left={"280px"}
            phone_left={"15px"}
            fs={"3rem"}
            fw={"600"}
            margin={"auto"}
            phone_fs={"2.5rem"}
          >
            Informasi
          </HeaderTitle>
          <StyleImage>
            <Image src={"/enam.jpg"} width={width} height={"350px"} />
          </StyleImage>
        </FlexRow>
        <FlexRow size={7} margin={"auto"}>
          <HeaderTitle
            width={"100%"}
            fs={"1.5rem"}
            fw={"600"}
            color={theme.secondary}
            phone_fs={"1.5rem"}
            padding={"5px"}
          >
            TERM & CONDITION
          </HeaderTitle>
          <Description
            padding={"12px"}
            width={"100%"}
            textAlign={"left"}
            fs={"1rem"}
            phone_width={"100%"}
            phone_pb={"20px"}
          >
            - Dasar perhitungan berat diambil dari mana yang lebih besar antara
            berat aktual dan berat Dimensi (rumus Berat Dimensi: Panjang x Lebar
            x tinggi /5.000)
          </Description>
          <Description
            padding={"12px"}
            width={"100%"}
            textAlign={"left"}
            fs={"1rem"}
            phone_width={"100%"}
            phone_pb={"20px"}
          >
            - Pembayaran, setelah penerbitan AWB biaya dibayarkan di muka Kami
            tidak menerima segala jenis claim sementara / claim pasti atas
            barang dengan kondisi :
          </Description>
          <FlexRow size={7} padding={"10px"}>
            <List>
              {informasi.map((el) => (
                <li>{el.name}</li>
              ))}
            </List>
          </FlexRow>
          <HeaderTitle
            width={"100%"}
            fs={"1.5rem"}
            fw={"600"}
            color={theme.secondary}
            phone_fs={"1.5rem"}
            padding={"5px"}
          >
            Frequently Asked Questions
          </HeaderTitle>
          <FlexRow size={7} padding={"10px"}>
            <Accordion data={data} />
          </FlexRow>
        </FlexRow>
      </FlexContainer>
    </Layout>
  );
}

export default Informasi;
