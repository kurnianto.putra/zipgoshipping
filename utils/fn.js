export function getLinkWhastapp(number, message) {
  return window.open(
    `https://api.whatsapp.com/send?phone=62${number}&text=${encodeURIComponent(
      message
    )}`
  );
}

export function checkPrice(number, data, negara) {
  const text = encodeURIComponent(
    "Halo admin zapgo, saya ingin menanyakan estimasi biaya pengantaran paket atas nama"
  );
  return window.open(
    `https://api.whatsapp.com/send?phone=62${number}&text=${text}:%0A%0ANama%20:${data.username}%0AEmail%20:${data.email}%0ANomor%20Telepon%20:${data.nomor_hp}%0ATujuan%20:%20${negara}%0AJenis%20Barang%20:%20${data.jenis}%0ABerat%20Barang%20:%20${data.berat} (kg) %0APanjang%20Barang%20:%20${data.panjang} (cm)%0ALebar%20Barang%20:%20${data.lebar} (cm)%0ATinggi%20Barang%20:%20${data.tinggi} (cm)`
  );
}

export const regNumber = (val) => {
  const reg = /^[0-9\b]+$/;
  if (reg.test(val)) {
    return val;
  }
};
export const searchAllValue = (data, field, values) => {
  let searchField = field;
  let searchVal = values;
  if (data !== null && searchAllValue && searchField) {
    return (
      data &&
      data.filter((el) =>
        el[searchField]
          .toLocaleLowerCase()
          .includes(searchVal.toLocaleLowerCase())
      )
    );
  }
};