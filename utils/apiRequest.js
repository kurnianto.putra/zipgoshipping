function handleResponse(response) {
  return response.text().then((text) => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      const error = text && JSON.parse(text);
      return Promise.reject(error);
    }
    return data;
  });
}

let request = async (method, url, body) => {
  return await fetch(`${url}`, {
    method,
    body,
    cache: "no-cache",
    credentials: "include",
    mode: "cors",
  }).then(handleResponse);
};

export const POST = () => request("POST", url, JSON.stringify(body));
export const GET = () => request("GET", url);
