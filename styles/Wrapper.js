import styled from "styled-components";
import { media } from "./Media";
export const FlexContainer = styled.div`
  background: ${(p) => p.background};
  flex-direction: ${(p) => p.flexDir};
  width: ${(p) => p.width || "100%"};
  position: ${(p) => p.position};
  z-index: ${(p) => p.zindex};
  overflow: ${(p) => p.overflow || "hidden"};
  display: flex;
  margin-left: ${(p) => p.marginLeft};
  height: ${(p) => p.height || "100vh"};
  flex-wrap: ${(p) => p.wrap};
  justify-content: ${(p) => p.jcontent};
  align-items: ${(p) => p.aitem};
  align-content: ${(p) => p.acontent};
  margin-top: ${(p) => p.mtop};
  padding: ${(p) => p.padding};
  border: ${(p) => p.border};
  ${media.phone`
    order: ${(p) => p.phone_order};
    flex-direction: ${(p) => p.phone_flexDir};
    width :${(p) => p.phone_widht};
    margin-right: ${(p) => p.phone_mr};
    height:${(p) => p.phone_height};
    font-size:${(p) => p.phone_fs};
    margin-top: ${(p) => p.phone_mt || "0px"};
  `}

  fieldset {
    border: 3px dashed #fff;
    padding: 5px;
    border-radius: 5px;
    legend {
      padding: 5px;
      font-weight: 300;
      font-size: 1.2rem;
      color: ${({ theme }) => theme.secondary};
    }
  }
`;

export const FlexRow = styled.div`
  display: ${(p) => p.display};
  position: ${(p) => p.position};
  overflow: ${(p) => p.overflow};
  z-index: ${(p) => p.zindex};
  align-items: ${(p) => p.aitems};
  flex-grow: ${(p) => p.fgrow};
  flex-wrap: ${(p) => p.flexWrap};
  flex-direction: ${(p) => p.flexDir};
  border-top: ${(p) => p.bt};
  text-align: ${(p) => p.ta};
  border: ${(p) => p.border};
  width: ${(p) => (p.size / 12) * 100}vw;
  height: ${(p) => p.height};
  background-color: ${(p) => p.bgColor};
  padding: ${(p) => p.padding};
  margin: ${(p) => p.margin};
  margin-right: ${(p) => p.mright};
  margin-top: ${(p) => p.mtop};
  margin-bottom: ${(p) => p.mbottom};
  justify-content: ${(p) => p.jcontent};
  border-radius: ${(p) => p.bradius};
  box-shadow: ${(p) => p.shadow && "20px 20px 15px -30px #080c21"};

  ${media.phone`
    order: ${(p) => p.phone_order};
    width :${(p) => p.phone_widht || "100%"};
    margin-right: ${(p) => p.phone_mr};
    height: ${(p) => p.phone_height};
    padding:${(p) => p.phone_padding};
    font-size:${(p) => p.phone_fs};
    justify-content: ${(p) => p.phone_jcontent};
    flex-wrap: ${(p) => p.phone_flexWrap};
    margin-top: ${(p) => p.phone_mt || "0px"};
  `}
`;
