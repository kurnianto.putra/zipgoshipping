import styled from "styled-components";
import { media } from "./Media";

export const WrapperCard = styled.div`
  display: flex;
  flex-wrap: wrap;
  position: relative;
  margin: 0 auto;
  width: ${(p) => p.width};
  justify-content: center;
  ${media.phone`
    width: ${(p) => p.phone_width || "100%"};
  `}
`;

export const WrapperCardScroll = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 0 auto;
  width: auto;
  justify-content: center;
  ${media.phone`
  display: flex;
  justify-content : flex-start;
      flex-wrap: nowrap;
      overflow-x: auto;
      width: 100%;
  `}
`;

export const Card = styled.div`
  border-radius: 5px;
  box-shadow: 0px 10px 40px -20px hsl(229, 6%, 66%);
  padding: 15px;
  width: 350px;
  margin: 20px;
  position: relative;
  text-align: center;
  background: #fff;
  border-top: 3px solid ${({ theme }) => theme.primary};
  h2 {
    color: ${({ theme }) => theme.textDesc};
    font-size: 1.5rem;
    font-weight: 600;
  }
  p {
    margin: 0 auto;
    line-height: 2;
    height: 100px;
    color: hsl(229, 6%, 66%);
  }

  img {
    position: absolute;
    width: 50px;
    bottom: 0px;
    right: 0;
    padding: 5px;
    margin-top: 10px;
  }
  ${media.phone`
    width: 350px;
    height: 180px;

    p{
      width: 300px;
      height: 80px;
    }

    &:hover{
    }

    &:after{
      content: "";
  display: block;
  position: absolute;
  right: -2rem;
  width: 2rem;
  height: 1px;
  padding-left: 300px ;
  padding-right: 300px ;
    }
  `}
`;
