import styled from "styled-components";
import { media } from "./Media";
export const WrapInput = styled.div`
  position: relative;
  width: ${(p) => p.width};
  margin: 5px;

  label {
    font-size: 0.8rem;
    color: red;
    font-style: italic;
  }
  ${media.phone`
    width: ${(p) => p.phone_width};
  `}
`;
export const WrappIcon = styled.div`
  position: absolute;
  padding: 0px 10px;
  color: #aaa;
  top: 9px;
  width: 40px;

  svg {
    width: 18px;
  }
`;

export const WrapperSearch = styled.ul`
display: flex;
flex-direction: column;
border-radius: 5px 5px 0px 0px;
list-style: none;
padding: 2px;
height: auto;
background-color: #fff;
li{
  cursor: pointer;
  text-transform: capitalize;
  color : ${({theme})=> theme.textColorP};
  width: 100%;
  padding: 5px 30px;
  border-bottom: 1px dashed ${({theme})=> theme.primary};
&:hover{
  background-color: ${({theme})=> theme.secondary};
  color: #fff;
  border-bottom: 1px dashed ${({theme})=> theme.yellow};

}
}

`
export const Input = styled.input`
  width: 100%;
  height: 40px;
  border: 1px solid #aaa;
  border-radius: 4px;
  outline: none;
  padding-left: ${(p) => p.padLeft || "45px"};

  &:focus {
    border: 1px solid ${({ theme }) => theme.secondary};
  }

  ${media.phone`
    padding-left: ${(p) => p.phone_padLeft};

  `}
`;
