import styled, { keyframes } from "styled-components";
import { media } from "./Media";

export const popIn = keyframes`
0%{
  opacity: 0;
  transform: translateY(-4rem)scale(.8);
}
100%{
opacity: 1;
transform: none;
}
`;
export const HeaderTitle = styled.h3`
  margin: ${(p) => p.margin};
  position: ${(p) => p.position};
  padding: ${(p) => p.padding};
  display: flex;
  flex-direction: column;
  bottom: ${(p) => p.bottom};
  left: ${(p) => p.left};
  width: ${(p) => p.width};
  font-size: ${(p) => p.fs};
  text-transform: ${(p) => p.textTransform};
  color: ${(p) => p.color || "#fff"};
  line-height: ${(p) => p.lineHeight};
  letter-spacing: ${(p) => p.letterSpac};
  text-align: ${(p) => p.textAlign};
  padding-bottom: ${(p) => p.pb};
  font-weight: ${(p) => p.fw};
  animation: ${popIn} 0.6s ease-out;
  span {
    margin: auto;
    font-size: 10px;
    font-weight: 300;
    color: ${({ theme }) => theme.textDesc};
  }
  hr {
    border: 0;
    width: 100px;
    height: 2px;
    margin: auto;
    margin-top: 10px;
    background-image: linear-gradient(
      to right,
      rgba(0, 0, 0, 0),
      rgba(2, 25, 53, 0.81),
      rgba(0, 0, 0, 0)
    );
  }
  ${media.phone`
  width: ${(p) => p.phone_width};
  font-size: ${(p) => p.phone_fs};
  line-height: ${(p) => p.phone_lineHeight};
  padding-bottom: ${(p) => p.phone_pb};
  left: ${(p) => p.phone_left};


  `}
`;

export const Description = styled.p`
  animation: ${popIn} 0.6s ease-out;
  font-size: ${(p) => p.fs || "12px"};
  padding: ${(p) => p.padding};
  width: ${(p) => p.width};
  margin-top: ${(p) => p.mt};
  margin: ${(p) => p.margin};
  text-align: ${(p) => p.textAlign};
  font-weight: ${(p) => p.fw || 300};
  color: ${(p) => p.color};
  ${media.phone`
  width: ${(p) => p.phone_width};
  font-size: ${(p) => p.phone_fs};
  line-height: ${(p) => p.phone_lineHeight};
  padding-bottom: ${(p) => p.phone_pb};
  `}
`;

export const Divider = styled.div`
  width: 100%;
  height: 40px;
  display: flex;
  padding-top: 35px;
  padding-bottom: 35px;
  justify-content: center;
  hr {
    border: none;
    height: 20px;
    width: 100%;
    animation: ${popIn} 0.9s ease-out;
    margin: 0 auto;
    background-image: url("/divider.svg");
    background-repeat: no-repeat;
    background-size: 80px;
    background-position: center;
  }
`;
